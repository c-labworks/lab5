﻿using System;

namespace lab5
{
    public abstract class Construction
    {
        protected string cipher { get; set; }
        protected string name { get; set; }
        protected DateTime issueDate { get; set; }

        public Construction()
        {
        }

        protected abstract void init(string cipher, string name, DateTime issueDate);

        protected virtual void translateName(string name)
        {
        }

        public Construction initInstance(string cipher, string name, DateTime issueDate)
        {
            translateName(name);
            init(cipher, name, issueDate);
            return this;
        }
        
        public string Name
        {
            get => name;

            set => name = value;
        }
        
        public string Cipher
        {
            get => cipher;

            set => cipher = value;
        }
        
        public DateTime IssueDate
        {
            get => issueDate;

            set => issueDate = value;
        }
    }
}