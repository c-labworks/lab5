﻿using System;

namespace lab5
{
    public class Pile : Construction
    {
        private string material { get; set; }
        private float diameter { get; set; }
        private string diveType { get; set; }

        public Pile(string material, float diameter, string diveType, string cipher, 
            string name, DateTime issueDate)
        {
            this.material = material;
            this.diameter = diameter;
            this.diveType = diveType;
            init(cipher, name, issueDate);
        }

        protected override void init(string cipher, string name, DateTime issueDate)
        {
            this.cipher = cipher;
            this.name = name;
            this.issueDate = issueDate;
        }
        
        public string Material
        {
            get => material;

            set => material = value;
        }
    }
}