﻿using System;

namespace lab5
{
    public class SOU : Construction
    {
        private double square { get; set; }
        private double resist { get; set; }

        public SOU(double square, double resist, string cipher, 
            string name, DateTime issueDate)
        {
            this.square = square;
            this.resist = resist;
            init(cipher, name, issueDate);
        }

        protected override void init(string cipher, string name, DateTime issueDate)
        {
            this.cipher = cipher;
            this.name = name;
            this.issueDate = issueDate;
        }
    }
}