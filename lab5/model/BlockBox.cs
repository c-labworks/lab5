﻿using System;

namespace lab5
{
    public class BlockBox : Construction
    {
        private double VRz { get; set; }
        private double VTin { get; set; }
        private double VR0 { get; set; }

        public BlockBox(double VRz, double VTin, double VR0, string cipher, 
            string name, DateTime issueDate)
        {
            this.VRz = VRz;
            this.VR0 = VR0;
            this.VTin = VTin;
            init(cipher, name, issueDate);
        }

        protected override void init(string cipher, string name, DateTime issueDate)
        {
            this.cipher = cipher;
            this.name = name;
            this.issueDate = issueDate;
        }
    }
}