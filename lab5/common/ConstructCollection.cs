﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace lab5
{
    public class ConstructCollection<T> where T : Construction
    {
        List<T> constructList = new List<T>();
        
        bool direction = false;
        
        public void ReverseDirection()
        {
            direction = !direction;
        }
        
        public List<T> getItems()
        {
            return constructList;
        }
        
        public void AddItem(T item)
        {
            this.constructList.Add(item);
        }
        
        public ConstructionIterator<T> GetEnumerator()
        {
            return new ConstructionIterator<T>(this, direction);
        }
        
        
        // private List<T> constructList;
        //
        // public ConstructCollection()
        // {
        //     constructList = new List<T>();
        // }
    }
}