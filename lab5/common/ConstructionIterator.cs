﻿using System.Collections;

namespace lab5
{
    public class ConstructionIterator<T> where T : Construction
    {
        private ConstructCollection<T> _collection;
        
        private int _position = -1;
        
        private bool _reverse = false;

        public ConstructionIterator(ConstructCollection<T> collection, bool reverse = false)
        {
            this._collection = collection;
            this._reverse = reverse;

            if (reverse)
            {
                this._position = collection.getItems().Count;
            }
        }
        
        public T Current()
        {
            return this._collection.getItems()[_position];
        }

        public  int Key()
        {
            return this._position;
        }
        
        public  bool MoveNext()
        {
            int updatedPosition = this._position + (this._reverse ? -1 : 1);

            if (updatedPosition >= 0 && updatedPosition < this._collection.getItems().Count)
            {
                this._position = updatedPosition;
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public  void Reset()
        {
            this._position = this._reverse ? this._collection.getItems().Count - 1 : 0;
        }
    }
}