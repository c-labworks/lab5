﻿namespace lab5
{
    public interface Iterator<T>
    {
        bool MoveNext();

        T Current { get; }

        void Reset();
    }
}