﻿using System;

namespace lab5
{
    public class Creator
    {
        private static Creator instance = new Creator(); //thread safe

        private Creator()
        {
        }

        public static Creator getInstance()
        {
            return instance;
        }

        public Pile createPile(string material, float diameter, string diveType, string cipher,
            string name, DateTime issueDate)
        {
            return new Pile(material, diameter, diveType, cipher, name, issueDate);
        }
    }
}