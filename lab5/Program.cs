﻿using System;
using System.Collections;

namespace lab5
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var collection = new ConstructCollection<Construction>();
            collection.AddItem(new Pile("steal", 12, "some-type", "asdasd",
                "nameOne", DateTime.Now));
            collection.AddItem(new BlockBox(1.2, 12, 4, "cvbcvb",
                "nameTwo", DateTime.Now));
            collection.AddItem(new SOU(1.2, 12, "bnmbnmb",
                "nameThree", DateTime.Now));
            
            var pileCollection = new ConstructCollection<Pile>();
            pileCollection.AddItem(new Pile("steal", 12, "some-type", "asdasd",
                "pileNameOne", DateTime.Now));
            pileCollection.AddItem(new Pile("steal", 12, "some-type", "asdasd",
                "pileNameTwo", DateTime.Now));


            ConstructionIterator<Construction> iterator = collection.GetEnumerator();
            while (iterator.MoveNext())
            {
                Console.WriteLine("Collection element. Name: " + iterator.Current().Name);
            }
            
            ConstructionIterator<Pile> pileIterator = pileCollection.GetEnumerator();
            while (pileIterator.MoveNext())
            {
                Console.WriteLine("Pile element. Name: " + pileIterator.Current().Name + ", Material: " + 
                                  pileIterator.Current().Material);
            }
        }
    }
}